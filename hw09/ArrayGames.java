////////////////////////////
/// CSE2 HW09
/// Jared Lee
/// April 16 2019
/// This lab will ask us if we want to insert or shorten method. 
/// If we choose the insert method, two inputs will be shown and one output array will show them both combined
/// If we choose the shorten method, there will be an input array and an integer input. Then the code will take away the array member that is associated with the integer input
import java.util.Scanner;
import java.util.Random;

public class ArrayGames{
  //This method will generate arrays with size between 10 and 20
  public static int[] generate(){
    Random random = new Random();
    int size = random.nextInt(11) + 10;//generates a size between 10 and 20
    int[] array = new int[size];//declare an array with a size between 10 and 20
    //create a for loop that will assign each array member with a random integer between 0 and 10
    for (int i = 0; i < size; i++){
      array[i] = random.nextInt(11);
    }
  return array;//return the array generated

  }
  //This method will print out the array 
  public static void print(int[] array){
    int size2 = array.length;//size2 equals the size of the array 
    int[] array2 = new int[size2];//declare new array with same size
    //use for loop to copy integers from array into array2
    for (int i = 0; i < size2; i++){
      array2[i] = array[i];
    }
    
    //use for loop here to list the integers in the array
    for (int i = 0; i < size2; i++){
     if (i == (size2 - 1)){
        System.out.print(array2[i]);
      }
      else {
        System.out.print(array2[i] + ",");
      }
    }
    
  }
  //this method will combine two arrays into one big array
  public static void insert(){
    Random random = new Random();
    int[] j = generate();// input 1 array 
    int[] k = generate();// input 2 array
    int size1 = j.length;//size of array1 is in size1
    int outputRandom = random.nextInt(size1);
    //input 1
    System.out.print("input 1: ");
    System.out.print("{");
    print(j);
    System.out.print("}");
    System.out.print("  ");
    //input 2
    System.out.print("input 2: ");
    System.out.print("{");
    print(k);
    System.out.print("}");
    System.out.println();
    //output
    System.out.print("Output: ");
    int[] outputArray = new int[size1];//declare new array for output
    //use for loop to list the combined arrays and their integers
    for (int i = 0; i < size1; i++){
      outputArray[i] = j[i];
      if (i == outputRandom) {// if the i is equal to the randomly generated number of the array member of array 1 in which array 2 will be put into
        if (i == 0){
          System.out.print("{");
          print(k);
          System.out.print(",");
          System.out.print(outputArray[i] + ",");
        }
       
        else if (i == size1 - 1){
          print(k);
          System.out.print(",");
          System.out.print(outputArray[i] + "}");
        } 
        else{
          print(k);
          System.out.print(",");
          System.out.print(outputArray[i] + ",");
        }
      } 
      else if (i != outputRandom){ // if i is not outputRandom
        if (i == 0){
          System.out.print("{" + outputArray[i] + ",");
        }
        else if (i == size1 - 1){
          System.out.print(outputArray[i] + "}");
        }
        else {
          System.out.print(outputArray[i] + ",");
        }
      }   
    }
    System.out.println();
  }
  //This program will use an array and integer input 
  public static void shorten(int input){
    Random random = new Random();
    int[] j = generate();//input 1
    int size1 = j.length;
    int input1 = input;
    //input 1
    System.out.print("input 1: ");
    System.out.print("{");
    print(j);
    System.out.print("}");
    System.out.print("  ");
    //input 2
    System.out.print("input 2: ");
    System.out.print(input1);
    System.out.println();
    //output 
    System.out.print("Output: ");
    int[] outputArray = new int[size1];//declare output array
    
    System.out.print("{");
    //use for loop array to initially make output array equal to input 1
    for (int i = 0; i < size1; i++){
      outputArray[i] = j[i];
      
      if ((input ==  size1 - 1) && (i == size1 - 2)){
        System.out.print(outputArray[i]);
      }
      if (i == size1 - 1){
        System.out.print(outputArray[i]);
      }
      
      if ((i == input) && (input != size1 - 1)) {
        System.out.print("");//this gets wil of the array member
      }
      else if (i != size1 - 1){
        System.out.print(outputArray[i] + ",");
      }//if i is not one of the array members, the array is left alone
    }
    System.out.print("}");
    System.out.println();
  }
  
  
  
  
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    System.out.print("Would you like to use the insert method or the shorten method ");//ask the user if they would like to use insert or shorten
    System.out.print("(Enter 'insert' or 'shorten'): ");
    String method = scan.next();
    
    if (method.equals("insert")){
      insert();//use the insert method
    }
    
    else if (method.equals("shorten")){
      System.out.print("Enter an integer input: ");//ask for an integer input
      int input = scan.nextInt();
      shorten(input);//run the shorten method with the integer input
    }
    
  }//end of main method
}//end of class method
