//////////////////////
/// CSE 02 Lab02
/// Jared Lee
/// 2/1/19
/// CSE 02
/* The program will do the following
    a.) Print the number of minutes for each trip
    b.) Print the number of counts fir each trip
    c.) Print the distance of each trip in miles 
    d.) Print the distance for the two trips combined */
public class Cyclometer {
    //main method required for every Java program
    public static void main(String[] args) {
        // our imput data. 
        int secsTrip1 = 480; // This is the number of seconds for trip 1
        int secsTrip2 = 3220; // This is the number of seconds for trip 2
        int countsTrip1 = 1561; // This is the number of counts for trip 1
        int countsTrip2 = 9037; // This is the number of counts for trip 2
        
        //out intermediate variables and output data
        double wheelDiameter = 27.0, // This is the wheel diameter
        PI = 3.14159, // This is the value of pi
        feetPerMile = 5280, // This is the number of feet in one mile
        inchesPerFoot = 12, // This is the number of inches per foot
        secondsPerMinute = 60; // This is the number of seconds per minute
        double distanceTrip1, distanceTrip2, totalDistance; // Turning these 3 variables into double variables
        
        System.out.println("Trip 1 took " + (secsTrip1 / secondsPerMinute) +
            " minutes and had " + countsTrip1 + " counts.");
        System.out.println("Trip 2 took " + (secsTrip2 / secondsPerMinute) +
            " minutes and had " + countsTrip2 + " counts.");
            
        distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
    	distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance in miles
    	distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	    totalDistance = distanceTrip1 + distanceTrip2;
	    
	    //Print out the output data.
        System.out.println("Trip 1 was "+ distanceTrip1 +" miles");
        System.out.println("Trip 2 was "+ distanceTrip2 +" miles");
        System.out.println("The total distance was " + totalDistance + " miles");

    } // end of main method
} // end of class
                                        
