///////////////////////////////
/// CSE2 LAB08
/// Jared Lee
/// 4/5/2019
/// This lab will give us an array with a size between 50 and 100 filled with random integers (0-99)
/// Then this program will give us the range as well as the standard deviation, mean, and a shuffled version of the array
import java.util.Arrays;
import java.util.Random;
import java.lang.Math;
public class Array{
  //this method will help us find the range 
  public static void getRange(int myArray[]){
    int size2 = myArray.length; //size = size of original array
    int[] array2 = new int [size2]; //declare array with size of original array
    //for loop copies the array from main method and makes it equal to array2
    for (int i = 0; i < size2; i++){
      array2[i] = myArray[i];
    }
    //initially say the min and max method are at array2[0]
    int max = array2[0];
    int min = array2[0];
    
    //max
    for (int j = 0; j < size2; j++){
      if (array2[j] > max){
        max = array2[j];
      }
    }
    
    //min
    for (int k = 0; k < size2; k++){
      if (array2[k] < min){
        min = array2[k];
      }
    }
    int range = max - min;
    System.out.print("Max: " + max);
    System.out.println();
    System.out.print("Min: " + min);
    System.out.println();
    System.out.println("Range: " + range);
    System.out.println();
    
  }
  //this method will print the mean of values in the array
  public static void getMean(int myArray[]) {
    int n = myArray.length;//same size as array in main method
    int[] array3 = new int [n];//declare new array 
    //copy array from original 
    for (int i = 0; i < n; i++){
      array3[i] = myArray[i];
    }
    double sum = 0.0;
    //use for loop to continuosly add all the values in array
    for (int j = 0; j < n; j++) {
      sum = sum + array3[j];
    }
    double mean = sum / n;
    System.out.println("Mean: " + mean);
    System.out.println();
  }
    
  //this method will print the standard deviation
  public static void getStdDev (int myArray[]) {
    int n = myArray.length;//same size as original array
    int[] array4 = new int [n];//declare new array
    //use for loop to copy original array
    for (int i = 0; i < n; i++){
      array4[i] = myArray[i];
    }
    //same code as mean
    double sumMean = 0.0;
    for (int j = 0; j < n; j++) {
      sumMean = sumMean + array4[j];
    }
    double mean = sumMean / n;
    double stdDev1 = 0.0;
    double stdDev2 = 0.0;
    double stdDev3 = 0.0;
    double stdDev4 = 0.0;
    //use for loops and Math.pow() to find standard deviation
      for (int k = 0; k < n; k++){
        stdDev1 =  (array4[k] - mean);
        stdDev2 = stdDev2 + Math.pow(stdDev1, 2);
      }
    stdDev3 = (stdDev2 / (n - 1));
    stdDev4 = Math.pow(stdDev3, 0.5);
    
    System.out.println("Standard Deviation: " + stdDev4);
    System.out.println();
  }
  
  //this method will create and print a shuffled version of the array
  public static void shuffle (int myArray[], int integer){
    Random random1 = new Random();
    int size5 = myArray.length;//size5 is same size as original array
    int[] array5 = new int [size5];//declare new array 
    // in for loop, var is a different random integer each time.
    for (int i = 0; i < size5; i ++) {
      int var = random1.nextInt(integer);
      //array5 will be equal to random value in original array
      array5[i] = myArray[var];
      System.out.println("Shuffle value: " + i + ": " + array5[i]);
    }
    System.out.println();
  }
  
  //this methods will list and print the array
  public static void printArray(int myArray[]) {
    for (int i = 0; i < myArray.length; i++){
      System.out.println("value " + i + ": "  + myArray[i]);
      
    }
    System.out.println();
  }
  
  // this is the main methid
  public static void main(String[] args){
    Random random = new Random(); //this will declare a variable that will generate a random number
    int integer = random.nextInt(51) + 50; //integer will be ranged between 50 and 100
    int[] myArray = new int [integer]; //declare an array with a size equal to variable integer
    int size = myArray.length; 
    //use for loop to populate array with integers between 0 and 99 inclusive 
    for (int i = 0; i < integer; i++) {
      myArray[i] = random.nextInt(100);
    }
    printArray(myArray);//prints the array
    getRange(myArray);//prints the range
    getMean(myArray);//prints the mean 
    getStdDev(myArray);//prints the standard deviation 
    shuffle(myArray, integer); //prints the shuffled version of original array
  }// end of main method

}// end of class method