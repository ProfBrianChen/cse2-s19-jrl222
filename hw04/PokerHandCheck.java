/////////////////////////////////////////
//// CSE 02 Lab 04
//// Jared Lee
//// 2/15/2019
//// This program will generate a card from a deck of 52 cards
//// It will print the suit and the 
// Make a class for CardGenerator
//import java.util.Scanner;

public class PokerHandCheck{
  public static void main(String[] args) {
    //Scanner myScanner = new Scanner( System.in );
    //Generate a random number between 0 and 1
    //Multiply that number by 52 and add 1
    int card = (int)((Math.random() * 52) + 1);
    String suit; 
    //Create parameters with random numbers generated to determine card suit
    //System.out.println("Enter the number on the card ");
    //int card = myScanner.nextInt();
   
    if (card >= 1 && card <= 13){
      suit = "Diamonds";
    }
    else if (card >= 14 && card <= 26) {
      suit = "Clubs";
    }
    else if (card >= 27 && card <= 39) {
      suit = "Hearts";
    }
    else {
      suit = "Spades";
    }
    
    String number;
    //cardNumber will equal the remainder of card % 13 to avoid creating multiple switch statements
    int cardNumber = card % 13;
    //Use switch statement to add in card numbers based on equation above
    switch ( cardNumber ) {
      case 1:
        number = "Ace";
        break;
      case 2:
        number = "2";
        break;
      case 3:
        number = "3";
        break;
      case 4:
        number = "4";
        break;
      case 5:
        number = "5";
        break;
      case 6:
        number = "6";
        break;
      case 7:
        number = "7";
        break;
      case 8:
        number = "8";
        break;
      case 9:
        number = "9";
        break;
      case 10:
        number = "10";
        break;
      case 11:
        number = "Jack";
        break;
      case 12:
        number = "Queen";
        break;
      default:
        number = "King";
        break;
    }
   //Repeat this code four more times for four different cards
    int card1 = (int)((Math.random() * 52) + 1);
    String suit1; 
    //Create parameters with random numbers generated to determine card suit
    //System.out.println("Enter the number on the card ");
    //int card1 = myScanner.nextInt();
   
    if (card1 >= 1 && card1 <= 13){
      suit1 = "Diamonds";
    }
    else if (card1 >= 14 && card1 <= 26) {
      suit1 = "Clubs";
    }
    else if (card1 >= 27 && card1 <= 39) {
      suit1 = "Hearts";
    }
    else {
      suit1 = "Spades";
    }
    
    String number1;
    //cardNumber will equal the remainder of card % 13 to avoid creating multiple switch statements
    int cardNumber1 = card1 % 13;
    //Use switch statement to add in card numbers based on equation above
    switch ( cardNumber1 ) {
      case 1:
        number1 = "Ace";
        break;
      case 2:
        number1 = "2";
        break;
      case 3:
        number1 = "3";
        break;
      case 4:
        number1 = "4";
        break;
      case 5:
        number1 = "5";
        break;
      case 6:
        number1 = "6";
        break;
      case 7:
        number1 = "7";
        break;
      case 8:
        number1 = "8";
        break;
      case 9:
        number1 = "9";
        break;
      case 10:
        number1 = "10";
        break;
      case 11:
        number1 = "Jack";
        break;
      case 12:
        number1 = "Queen";
        break;
      default:
        number1 = "King";
        break;
    }
    
    int card2 = (int)((Math.random() * 52) + 1);
    String suit2; 
    //Create parameters with random numbers generated to determine card suit
    //System.out.println("Enter the number on the card ");
    //int card2 = myScanner.nextInt();
   
    if (card2 >= 1 && card2 <= 13){
      suit2 = "Diamonds";
    }
    else if (card2 >= 14 && card2 <= 26) {
      suit2 = "Clubs";
    }
    else if (card2 >= 27 && card2 <= 39) {
      suit2 = "Hearts";
    }
    else {
      suit2 = "Spades";
    }
    
    String number2;
    //cardNumber will equal the remainder of card % 13 to avoid creating multiple switch statements
    int cardNumber2 = card2 % 13;
    //Use switch statement to add in card numbers based on equation above
    switch ( cardNumber2 ) {
      case 1:
        number2 = "Ace";
        break;
      case 2:
        number2 = "2";
        break;
      case 3:
        number2 = "3";
        break;
      case 4:
        number2 = "4";
        break;
      case 5:
        number2 = "5";
        break;
      case 6:
        number2 = "6";
        break;
      case 7:
        number2 = "7";
        break;
      case 8:
        number2 = "8";
        break;
      case 9:
        number2 = "9";
        break;
      case 10:
        number2 = "10";
        break;
      case 11:
        number2 = "Jack";
        break;
      case 12:
        number2 = "Queen";
        break;
      default:
        number2 = "King";
        break;
    }
    
    int card3 = (int)((Math.random() * 52) + 1);
    String suit3; 
    //Create parameters with random numbers generated to determine card suit
    //System.out.println("Enter the number on the card ");
    //int card3 = myScanner.nextInt();
   
    if (card3 >= 1 && card3 <= 13){
      suit3 = "Diamonds";
    }
    else if (card3 >= 14 && card3 <= 26) {
      suit3 = "Clubs";
    }
    else if (card3 >= 27 && card3 <= 39) {
      suit3 = "Hearts";
    }
    else {
      suit3 = "Spades";
    }
    
    String number3;
    //cardNumber will equal the remainder of card % 13 to avoid creating multiple switch statements
    int cardNumber3 = card3 % 13;
    //Use switch statement to add in card numbers based on equation above
    switch ( cardNumber3 ) {
      case 1:
        number3 = "Ace";
        break;
      case 2:
        number3 = "2";
        break;
      case 3:
        number3 = "3";
        break;
      case 4:
        number3 = "4";
        break;
      case 5:
        number3 = "5";
        break;
      case 6:
        number3 = "6";
        break;
      case 7:
        number3 = "7";
        break;
      case 8:
        number3 = "8";
        break;
      case 9:
        number3 = "9";
        break;
      case 10:
        number3 = "10";
        break;
      case 11:
        number3 = "Jack";
        break;
      case 12:
        number3 = "Queen";
        break;
      default:
        number3 = "King";
        break;
    }
    
    int card4 = (int)((Math.random() * 52) + 1);
    String suit4; 
    //Create parameters with random numbers generated to determine card suit
    //System.out.println("Enter the number on the card ");
    //int card4 = myScanner.nextInt();
   
    if (card4 >= 1 && card4 <= 13){
      suit4 = "Diamonds";
    }
    else if (card4 >= 14 && card4 <= 26) {
      suit4 = "Clubs";
    }
    else if (card4 >= 27 && card4 <= 39) {
      suit4 = "Hearts";
    }
    else {
      suit4 = "Spades";
    }
    
    String number4;
    //cardNumber will equal the remainder of card % 13 to avoid creating multiple switch statements
    int cardNumber4 = card4 % 13;
    //Use switch statement to add in card numbers based on equation above
    switch ( cardNumber4 ) {
      case 1:
        number4 = "Ace";
        break;
      case 2:
        number4 = "2";
        break;
      case 3:
        number4 = "3";
        break;
      case 4:
        number4 = "4";
        break;
      case 5:
        number4 = "5";
        break;
      case 6:
        number4 = "6";
        break;
      case 7:
        number4 = "7";
        break;
      case 8:
        number4 = "8";
        break;
      case 9:
        number4 = "9";
        break;
      case 10:
        number4 = "10";
        break;
      case 11:
        number4 = "Jack";
        break;
      case 12:
        number4 = "Queen";
        break;
      default:
        number4 = "King";
        break;
    }
    //Initiate String combo
    String combo = "";
    //Input all possibilities that would get you three of a kind, two pairs, a pair, and a high card hand respectively
    if (((cardNumber == cardNumber1) && (cardNumber == cardNumber2)) || ((cardNumber == cardNumber1) && (cardNumber == cardNumber3)) || ((cardNumber == cardNumber1) && (cardNumber == cardNumber4)) || ((cardNumber == cardNumber2) && (cardNumber == cardNumber3)) || ((cardNumber == cardNumber2) && (cardNumber == cardNumber4)) || ((cardNumber == cardNumber3) && (cardNumber == cardNumber4)) || ((cardNumber1 == cardNumber2) && (cardNumber1 == cardNumber3)) || ((cardNumber1 == cardNumber2) && (cardNumber1 == cardNumber4)) || ((cardNumber1 == cardNumber3) && (cardNumber1 == cardNumber4)) || ((cardNumber2 == cardNumber3) && (cardNumber2 == cardNumber4))) {
      combo = "three of a kind!";
    }
    else if (((cardNumber == cardNumber1) && (cardNumber2 == cardNumber3)) || ((cardNumber == cardNumber2) && (number1 == number3)) || ((number == number3) && (number1 == number2)) || ((number == number1) && (number2 == number4)) || ((number == number2) && (number1 == number4)) || ((number == number4) && (number1 == number2)) || ((number == number1) && (number3 == number4)) || ((number == number4) && (number1 == number3)) || ((number1 == number3) && (number2 == number4)) || ((number1 == number4) && (number2 == number3)) || ((number == number2) && (number3 == number4)) || ((number == number3) && (number2 == number4)) || ((number1 == number2) && (number3 == number4)) || ((number1 == number3) && (number2 == number4)) || ((number1 == number4) && (number2 == number3))) {
      combo = "two pairs!";
    }
   else if ((cardNumber == cardNumber1) || (cardNumber == cardNumber2) || (cardNumber == cardNumber3) || (cardNumber == cardNumber4) || (cardNumber1 == cardNumber2) || (cardNumber1 == cardNumber3) || (cardNumber1 == cardNumber4) || (cardNumber2 == cardNumber3) || (cardNumber2 == cardNumber4) || (cardNumber3 == cardNumber4)) {
       combo = "pair!";
    }
    else {
      combo = "high card hand!";
    }
    
    //Print out five different cards and print the type of combination you have
    System.out.println("Your random cards were: ");
    System.out.println("  The " + number + " of " + suit);
    System.out.println("  The " + number1 + " of " + suit1);
    System.out.println("  The " + number2 + " of " + suit2);
    System.out.println("  The " + number3 + " of " + suit3);
    System.out.println("  The " + number4 + " of " + suit4);
    System.out.println("  You have a " + combo);
  }//end of main method
  
}//end of class
             