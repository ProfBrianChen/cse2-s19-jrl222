///////////////////////
//// CSE 02 hw03
//// Jared Lee
//// 2/5/19
//// This program will function to be like an arithmetic calculator.This
//// This program uses double and int variables to label prices and numbers of items
//// The program also uses int so that the prices can only have two significant past the decimal 
//// Ultimately this program will find the total cost of each kind of item, the sales tax charged for each item,
//// total cost of purchases, total sales tax, and total paid for this transaction. 
public class Arithmetic {
  public static void main (String[] args) {
  //the tax rate
  double paSalesTax = 0.06;
  
  //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;
  //total costs of pantsPrice
  double totalCostOfPants = pantsPrice * numPants;
  //sales tax charged on pants
  //multiply sales tax by 100, cast to int, and divide by 100.0 to get only two significant figures after the decimal.
  double salesTaxOfPants = totalCostOfPants * paSalesTax * 100;
  salesTaxOfPants = (int) salesTaxOfPants;
  salesTaxOfPants = salesTaxOfPants / 100.0;

  //Number of sweatshirts
  int numShirts = 2;
  //Cost per shirt
  double shirtPrice = 24.99;
  //total costs of sweatshirts
  double totalCostOfSweatShirts = numShirts * shirtPrice;
  //sales tax charged on sweatshirts
  ////multiply sales tax by 100, cast to int, and divide by 100.0 to get only two significant figures after the decimal.
  double salesTaxOfSweatShirts = totalCostOfSweatShirts * paSalesTax * 100;
  salesTaxOfSweatShirts = (int) salesTaxOfSweatShirts;
  salesTaxOfSweatShirts = salesTaxOfSweatShirts / 100.0;

  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltCost = 33.99;
  //total costs of belts
  double totalCostOfBelts = numBelts * beltCost;
  //sales tax charged on belts
  ////multiply sales tax by 100, cast to int, and divide by 100.0 to get only two significant figures after the decimal.
  double salesTaxOfBelts = totalCostOfBelts * paSalesTax * 100; 
  salesTaxOfBelts = (int) salesTaxOfBelts;
  salesTaxOfBelts = salesTaxOfBelts / 100.0; 
 
  
  
  //total cost of purchases before sales tax
  double totalCostOfPurchases = totalCostOfPants + totalCostOfSweatShirts + totalCostOfBelts;
  
  //total sales tax
  double totalSalesTax = salesTaxOfPants + salesTaxOfSweatShirts + salesTaxOfBelts;
  
  //total paid for this transaction
  double totalTransaction = (totalCostOfPants + salesTaxOfPants) + 
    (totalCostOfSweatShirts + salesTaxOfSweatShirts) + 
    (totalCostOfBelts + salesTaxOfBelts);

    System.out.println( "The total costs of pants is " + totalCostOfPants + " dollars.");
    System.out.println( "The total costs of sweatshirts is " + totalCostOfSweatShirts + " dollars.");
    System.out.println( "The total costs of belts is " + totalCostOfBelts + " dollars.");
    
    System.out.println( "The sales tax charged on pants is " + salesTaxOfPants + " dollars.");
    System.out.println( "The sales tax charged on sweatshirts is " + salesTaxOfSweatShirts + " dollars.");
    System.out.println( "The sales tax charged on belts is " + salesTaxOfBelts + " dollars.");
    
    System.out.println( "The total costs of purchases is " + totalCostOfPurchases + " dollars.");
    
    System.out.println( "The total sales tax is " + totalSalesTax + " dollars.");
    
    System.out.println( "The total paid for this transaction, including sales tax, is " + totalTransaction + " dollars.");
    
  }
}