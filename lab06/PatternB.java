/////////////////////////////////
//// CSE2 LAB06
//// Jared Lee
//// 3/8/2019
//// This program will create a pattern in the shape of pattern B. The size of the triangle depends on the input
// import this scanner class so that we put in inputs
import java.util.Scanner;

public class PatternB {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    System.out.println("Enter an integer from 1 to 10: ");
    int i = 0;
    while (i == 0) {
       if (myScanner.hasNextInt()) {
       i = myScanner.nextInt();
        // if integer is a negative or greater than 10, reenter integer.
        if ((i < 0) || (i > 10)) {
          System.out.println("Please reenter an integer.");
          i = 0;
       }
       }
      // if an something other than an integer is inputted, reenter integer
     else {
       System.out.println("Please reenter an integer.");
       String junk = myScanner.next();
       //clears whatever is in myScanner
      }
    }
    
    //initialize i as an integer
    int number = i;
    // create a for loop associated with the rows in the pattern
    for (int numRows = 0; numRows <= i; numRows++) {
      // create a for loop for numbers being inputted in each row
      for (int j = 1; j <= number; j++) {
        System.out.print(j + " ");
      }
      // create this if statement so that there is no space after pattern is done
      if (numRows < i) {
        System.out.println();
      }
      // decrease number for every loop so that the number of numbers in each consecutive row decreases
      number--;
  
      
    }
  
  }
}