/////////////////////////////////
//// CSE2 LAB06
//// Jared Lee
//// 3/8/2019
//// This program will create a pattern in the shape of pattern A. The size of the triangle depends on the input.
// import this scanner class so that we put in inputs
import java.util.Scanner;

public class PatternA {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    System.out.println("Enter an integer from 1 to 10: ");
    // initialize i as an int 
    int i = 0;
    while (i == 0) {
       if (myScanner.hasNextInt()) {
       i = myScanner.nextInt();
        // if the input is an integer that is a negative or greater than 10, reenter an input
        if ((i < 0) || (i > 10)) {
          System.out.println("Please reenter an integer.");
          // make i = 0 so that a different integer can be inputted
          i = 0;
       }
       }
      // if the input not an integer, reenter an integer
      else {
       System.out.println("Please reenter an integer.");
       //clears whatever is in myScanner
       String junk = myScanner.next();
      }
    }
    
    //initialize pattern as a string
    String pattern = "";
    //create a for loop associated with the rows in the pattern
    for (int numRows = 0; numRows < i; numRows++) {
      // when numRows = 0, pattern starts off with 1
      if (numRows == 0) {
        pattern = "1";
      } 
      // when numRows is not 0, add a number that'c incrementing by one
      else {
        pattern = pattern + " " + (numRows + 1);
      }
      // everytime pattern is printed, the text editor immediately starts on a new line
      System.out.println(pattern);
    }
  
  }//end of main method
}//end of class method