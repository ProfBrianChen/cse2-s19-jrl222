/////////////////////////////////
//// CSE2 LAB06
//// Jared Lee
//// 3/8/2019
//// This program will create a pattern in the shape of pattern D. The size of the triangle depends on the input
// import this scanner class so that we put in inputs
import java.util.Scanner;

public class PatternD {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    System.out.println("Enter an integer from 1 to 10: ");
    int i = 0;
    while (i == 0) {
       if (myScanner.hasNextInt()) {
       i = myScanner.nextInt();
         //if the integer is negative or greater than 10, reenter integer
        if ((i < 0) || (i > 10)) {
          System.out.println("Please reenter an integer.");
          i = 0;
       }
       }
      // if the input is not an integer, reenter an integer
     else {
       System.out.println("Please reenter an integer.");
       String junk = myScanner.next();
       //Clears whatever is in myScanner
      }
    }
    
    //intialize number as integer and make it equal to i
    //This code is the same as Pattern B except j equals number at the start, for loop runs till j is less than 0, and j decreases
    int number = i;
    //create for loop for the rows in the pattern
    for (int numRows = 0; numRows <= i; numRows++) {
      //create for loop for numbers being in each row
      for (int j = number; j > 0; j--) {
        System.out.print(j + " ");
      }
      // when loop is finished, start on a new line
      // This also prevents a new line after the pattern is finished
      if (numRows < i) {
        System.out.println();
      }
      number--;
      // decrease number for each loop
  
      
    }
   
      
  
    }
  
  }