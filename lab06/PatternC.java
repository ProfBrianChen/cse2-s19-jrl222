/////////////////////////////////
//// CSE2 LAB06
//// Jared Lee
//// 3/8/2019
//// This program will create a pattern in the shape of pattern D. The size of the triangle depends on the input
// import this scanner class so that we put in inputs
import java.util.Scanner;

public class PatternC {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    System.out.println("Enter an integer from 1 to 10: ");
    int i = 0;
    while (i == 0) {
       if (myScanner.hasNextInt()) {
       i = myScanner.nextInt();
         //if integer is a negative or greater than 10, reenter integer
        if ((i < 0) || (i > 10)) {
          System.out.println("Please reenter an integer.");
          i = 0;
       }
       }
      //if input is anything but an integer, reenter integer
     else {
       System.out.println("Please reenter an integer.");
       String junk = myScanner.next();
       //clears whatever is in myScanner
      }
    }
    
  // initialize number as an integer and make it equal to i
  int number = i;
    //create for loop for the rows in the pattern
    for (int numRows = 0; numRows <= i; numRows++) {
      //create for loops for spaces and numbers in each row
      for (int j = 1; j <= i; j++) {
        // if j is less than number, create spaces
        if (j < number) {
          System.out.print(" ");
        }
        //only if j is greater than or equal to number, print the difference of i and j-1 right next to each other
        else if (j >= number) {
          System.out.print((i - (j-1)));
          
        }
      }
      //if numRows is less than i after the for loop, start a new line in text editor
      if (numRows < i) {
        System.out.println();
      }
      number--;
      //decrease number everytime for loop loops
    }
  
      
    
  }
}