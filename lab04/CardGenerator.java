/////////////////////////////////////////
//// CSE 02 Lab 04
//// Jared Lee
//// 2/15/2019
//// This program will generate a card from a deck of 52 cards
//// It will print the suit and the 
// Make a class for CardGenerator
public class CardGenerator{
  public static void main(String[] args) {
    //Generate a random number between 0 and 1
    //Multiply that number by 52 and add 1
    int card = (int)((Math.random() * 52) + 1);
    String suit; 
    //Create parameters with random numbers generated to determine card suit
    if (card >= 1 && card <= 13) {
      suit = "Diamonds";
    }
    else if (card >= 14 && card <= 26) {
      suit = "Clubs";
    }
    else if (card >= 27 && card <= 39) {
      suit = "Hearts";
    }
    else {
      suit = "Spades";
    }
    String number;
     //cardNumber will equal the remainder of card % 13 to avoid creating multiple switch statements
    int cardNumber = card % 13;
    //Use switch statement to add in card numbers based on equation above
    switch ( cardNumber ) {
      case 1:
        number = "Ace";
        break;
      case 2:
        number = "2";
        break;
      case 3:
        number = "3";
        break;
      case 4:
        number = "4";
        break;
      case 5:
        number = "5";
        break;
      case 6:
        number = "6";
        break;
      case 7:
        number = "7";
        break;
      case 8:
        number = "8";
        break;
      case 9:
        number = "9";
        break;
      case 10:
        number = "10";
        break;
      case 11:
        number = "Jack";
        break;
      case 12:
        number = "Queen";
        break;
      default:
        number = "King";
        break;
    }
   
    System.out.println("You picked the " + number + " of " + suit);

  }//end of main method
  
}//end of class