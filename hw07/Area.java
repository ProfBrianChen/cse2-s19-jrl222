//////////////////////////
//// CSE2 HW07
//// Jared Lee
////3/26/2019
//// This program will ask for shape. Then it will ask for dimensions which the program will use to find the area.
import java.util.Scanner;

public class Area{
  //Method to check input for shapes
  public static String getShape() {
    Scanner scan = new Scanner (System.in);
    while(true){
      System.out.println("What shape would you like: ");
      // input the type of shape you want
      String shape = scan.next();
      if (shape.equals("circle")) {
        return shape;
      } else if(shape.equals("rectangle")) {
        return shape;
      } else if(shape.equals("triangle")) {
        return shape;
        //if shape is not one of the three, tell them not valid shape and choose again
      } else {
        System.out.print("That is not a valid shape. The options are a circle, rectangle, and a triangle. Please rechoose a shape.");
      }
    }
  }
  //method for finding the circle area
  public static double circArea (double r) {  
    //area of circle
    double circleArea = (r * r  * 2 * 3.1415);
    return circleArea;
  }
  // method for finding the rectangle area
  public static double rectArea(double width, double length) {
    //area of the rectangle
    double rectangleArea = width * length;
    return rectangleArea;
  }
  //method for finding the area of a triangle
  public static double triArea(double height, double base) {
    //area of a triangle
    double triangleArea = height * base * 0.5;
    return triangleArea;
  }
  //main method
  public static void main(String[] args) {
    Scanner scan = new Scanner (System.in);
    //The string shape will equal to what is returned from input method
    String shape = getShape();
    //if shape equals circle
    if (shape.equals("circle")) {
     Scanner radius = new Scanner (System.in);
      //for circle, find radius
     System.out.println("What is the radius of the circle: ");
     double r = 0;
     while (r == 0) {
      if (radius.hasNextDouble()) {
        r = radius.nextDouble();
        //radius cannot be 0 or negative
        if (r <= 0) {
          System.out.println("Reenter the radius.");
          r = 0;
        }
      }
    }
    double  c = circArea(r);
    System.out.printf("The area of the circle is %.2f %n", c);
   }
    //if shape equals triangel
   else if (shape.equals("triangle")) {   
     Scanner tri = new Scanner (System.in);
     //find height and base of triangle
     double height = 0;
     double base = 0;
     System.out.println("What is the height of the triangle: ");
     while (height == 0) {
       if (tri.hasNextDouble()) {
         height = tri.nextDouble();
         if (height <= 0) {
           System.out.println("Reenter a new height.");
           height = 0;
         }
       }
     }
     System.out.println("What is the base of the triangle: ");
     while (base == 0) {
       if (tri.hasNextDouble()) {
          base = tri.nextDouble();
          if (base <=0) {
            System.out.println("Reenter a new length.");
            base = 0;
          }
       }
     }
     //get returned area value from triangle method
     double t = triArea(height, base);
     System.out.printf("The area of the triangle is %.2f %n", t);

   }
    //if shape equals rectangle
   else if (shape.equals("rectangle")){
     Scanner rect = new Scanner (System.in);
     System.out.println("What is the width of the rectangle: ");
     //find the length of one side and another side
     double side1 = 0;
     double side2 = 0;
     while (side1 == 0) {
       if (rect.hasNextDouble()) {
         side1 = rect.nextDouble();
         if (side1 <= 0) {
         System.out.println("Reenter a new width.");
         side1 = 0;
         }
       }
     }
     System.out.println("What is the length of the rectangle: ");
     while (side2 == 0) {
       if (rect.hasNextDouble()) {
          side2 = rect.nextDouble();
          if (side2 <=0) {
            System.out.println("Reenter a new length.");
            side2 = 0;
          }
       }
     }
     //return the area of a rectangle from rectangle method
     double re = rectArea(side1, side2);
     System.out.printf("The area of the rectangle is %.2f %n", re);
   }
  }
  
}