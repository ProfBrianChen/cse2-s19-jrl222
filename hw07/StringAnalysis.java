////////////////////
///// CSE2 HW07
///// Jared Lee
///// 3/26/2019
///// This program will check the characters in a string and tell if it's a letter or not
import java.util.Scanner;
public class StringAnalysis{
  //main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a string: ");
    //input a string
    String myString = scan.next();
    int size = -1;
    while (size <0) {
      System.out.println("Enter the amount of characters to check. 0 to check the entire string: ");
       if (scan.hasNextInt()) {
         size = scan.nextInt();
         //if input is negative, reenter an integer
         if (size < 0) {
         System.out.println("Error: please type in an integer. ");
        }  
      } 
      //if integer is not inputted or anything other than it, reenter integer
      else {
       System.out.println("Please reenter an integer.");
       String junk = scan.next();
      }
    }
    if(size==0){
      //if size equals 0, the whole string contains letters
      if(stringChecker(myString)) {
        System.out.println("All characters are letters");
      } 
      else {
        System.out.println("Some characters are not letters");
      }
    } 
    else {
      if(stringChecker(myString,size)) {
        System.out.println("All characters are letters");
      } else {
        System.out.println("Some characters are not letters");
      }
    }
  }
  //method with only the string
  public static boolean stringChecker(String myString) {
    int i =0;
    while(i<myString.length()) {
      //When the character at position i is not a letter
      if(!Character.isLetter(myString.charAt(i))) {
       return false;
      }
      i++;
    }
    return true;
  }
  //method that takes string and integer
  public static boolean stringChecker(String myString, int size) {
    int i=0;
    while((i < size) && ( i < myString.length())) {
      //When the character at position i is not a letter
      if(!Character.isLetter(myString.charAt(i))) {
       return false;
      }
      i++;
    }
    return true;
  }
}