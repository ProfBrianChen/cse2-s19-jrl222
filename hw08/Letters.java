////////////////////////
/// CSE2 HW08
/// Jared Lee
/// 4/6/2019
/// This program will print out a random number of letters either capital or lower
/// Then the program will list the letters printed out that are from A to M or N to z

import java.util.Arrays;
import java.util.Random; 
public class Letters {
  //Declare method for characters 
  public static char[] Array( int x ) {
    Random random = new Random();
    //create a size for array
    int alphabet2 = random.nextInt(52);
    // declare array
    char[] letter;
     // make size for new array
     letter = new char[52];
      //Array for letter is listed
      letter[0] = 'A';
      letter[1] = 'B';
      letter[2] = 'C';
      letter[3] = 'D';
      letter[4] = 'E';
      letter[5] = 'F';
      letter[6] = 'G';
      letter[7] = 'H';
      letter[8] = 'I';
      letter[9] = 'J';
      letter[10] = 'K';
      letter[11] = 'L';
      letter[12] = 'M';
      letter[13] = 'N';
      letter[14] = 'O';
      letter[15] = 'P';
      letter[16] = 'Q';
      letter[17] = 'R';
      letter[18] = 'S';
      letter[19] = 'T';
      letter[20] = 'U';
      letter[21] = 'V';
      letter[22] = 'W';
      letter[23] = 'X';
      letter[24] = 'Y';
      letter[25] = 'Z';
      letter[26] = 'a';
      letter[27] = 'b';
      letter[28] = 'c';
      letter[29] = 'd';
      letter[30] = 'e';
      letter[31] = 'f';
      letter[32] = 'g';
      letter[33] = 'h';
      letter[34] = 'i';
      letter[35] = 'j';
      letter[36] = 'k';
      letter[37] = 'l';
      letter[38] = 'm';
      letter[39] = 'n';
      letter[40] = 'o';
      letter[41] = 'p';
      letter[42] = 'q';
      letter[43] = 'r';
      letter[44] = 's';
      letter[45] = 't';
      letter[46] = 'u';
      letter[47] = 'v';
      letter[48] = 'w';
      letter[49] = 'x';
      letter[50] = 'y';
      letter[51] = 'z';
    //create new array but with random size
      char[] letter2 = new char[alphabet2];
    //create for loop for 
      for (int i = 0; i < alphabet2; i++) {
        int var = random.nextInt(52);
        //makes sure the values of letter2 are randomized
        letter2[i] = letter[var];
      }
      return letter2;
    
  }
 // method for letters capital and not capital A to M
 public static char[] getAtoM(char[] Array){
    char[] letter;
     letter = new char[52];
    
      letter[0] = 'A';
      letter[1] = 'B';
      letter[2] = 'C';
      letter[3] = 'D';
      letter[4] = 'E';
      letter[5] = 'F';
      letter[6] = 'G';
      letter[7] = 'H';
      letter[8] = 'I';
      letter[9] = 'J';
      letter[10] = 'K';
      letter[11] = 'L';
      letter[12] = 'M';
      letter[13] = 'N';
      letter[14] = 'O';
      letter[15] = 'P';
      letter[16] = 'Q';
      letter[17] = 'R';
      letter[18] = 'S';
      letter[19] = 'T';
      letter[20] = 'U';
      letter[21] = 'V';
      letter[22] = 'W';
      letter[23] = 'X';
      letter[24] = 'Y';
      letter[25] = 'Z';
      letter[26] = 'a';
      letter[27] = 'b';
      letter[28] = 'c';
      letter[29] = 'd';
      letter[30] = 'e';
      letter[31] = 'f';
      letter[32] = 'g';
      letter[33] = 'h';
      letter[34] = 'i';
      letter[35] = 'j';
      letter[36] = 'k';
      letter[37] = 'l';
      letter[38] = 'm';
      letter[39] = 'n';
      letter[40] = 'o';
      letter[41] = 'p';
      letter[42] = 'q';
      letter[43] = 'r';
      letter[44] = 's';
      letter[45] = 't';
      letter[46] = 'u';
      letter[47] = 'v';
      letter[48] = 'w';
      letter[49] = 'x';
      letter[50] = 'y';
      letter[51] = 'z';
    for (int i = 0; i < Array.length; i++){
      //if statement makes sure only letters a to m are printed out
      if (((Array[i] >= letter[0]) && (Array[i] < letter[13])) ||((Array[i] > letter[25]) && (Array[i] < letter[39]))){
      System.out.print(Array[i]);
    }
    
    }
   return letter;
  }
  // method for letters n to z
  public static char[] getNtoZ(char[] Array){
    char[] letter;
     letter = new char[52];
    
      letter[0] = 'A';
      letter[1] = 'B';
      letter[2] = 'C';
      letter[3] = 'D';
      letter[4] = 'E';
      letter[5] = 'F';
      letter[6] = 'G';
      letter[7] = 'H';
      letter[8] = 'I';
      letter[9] = 'J';
      letter[10] = 'K';
      letter[11] = 'L';
      letter[12] = 'M';
      letter[13] = 'N';
      letter[14] = 'O';
      letter[15] = 'P';
      letter[16] = 'Q';
      letter[17] = 'R';
      letter[18] = 'S';
      letter[19] = 'T';
      letter[20] = 'U';
      letter[21] = 'V';
      letter[22] = 'W';
      letter[23] = 'X';
      letter[24] = 'Y';
      letter[25] = 'Z';
      letter[26] = 'a';
      letter[27] = 'b';
      letter[28] = 'c';
      letter[29] = 'd';
      letter[30] = 'e';
      letter[31] = 'f';
      letter[32] = 'g';
      letter[33] = 'h';
      letter[34] = 'i';
      letter[35] = 'j';
      letter[36] = 'k';
      letter[37] = 'l';
      letter[38] = 'm';
      letter[39] = 'n';
      letter[40] = 'o';
      letter[41] = 'p';
      letter[42] = 'q';
      letter[43] = 'r';
      letter[44] = 's';
      letter[45] = 't';
      letter[46] = 'u';
      letter[47] = 'v';
      letter[48] = 'w';
      letter[49] = 'x';
      letter[50] = 'y';
      letter[51] = 'z';
    for (int i = 0; i < Array.length; i++){
      //makes sure only letters n to z capital or not are printed
      if (((Array[i] >= letter[13]) && (Array[i] < letter[26])) ||((Array[i] > letter[38]) && (Array[i] <= letter[51]))){
      System.out.print(Array[i]);
    }
    
    }
   return letter;
  }
 
  // main method
  public static void main(String[] args) {
    char[] array2= Array(0);
    System.out.print("Random character array: ");
    for(int i = 0; i< array2.length; i++){
      System.out.print(array2[i]);
    }
    System.out.println();
    System.out.print("AtoM characters: ");
    //calls on AtoM method
    getAtoM(array2);
    System.out.println();
    System.out.print("NtoZ characters: ");
    //calls on NtoZ method
    getNtoZ(array2);
    System.out.println();
    
    }//end of main method
  
  }//end of class method
