///////////////////////////
/// CSE2 HW08 PART 2
/// Jared Lee
/// 4/8/2019
/// This program will ask the user to input 5 numbers and compare them to 5 randomly generated numbers from the program. Then it will determine if you win or lose.
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;
public class PlayLottery {
  // method for the numbers the user inputs
  public static int[] userPicked(){
    Scanner scan = new Scanner(System.in);
    //declare the user array
    int[] user;
    user = new int[5];
    int picks = 0;
    for (int i = 0; i < 5; i++){
      if (picks == 0){
        if (scan.hasNextInt()){
          user[i] = scan.nextInt();
        }
      }
    
  }
    return user;
  }
  //method for the computer generated numbers
  public static int[] numbersPicked() {
    Random random = new Random();
    int total = random.nextInt(60);
    //declare computer generated number array
    int[] number;
    number = new int[60];
    int[] number2;
    number2 = new int[total];
    
    for (int i = 0; i < 60; i++){
      number[i] = i;
    }
    
    for (int x = 0; x < total; x++) {
      int var = random.nextInt(60);
      number2[x] = number[var];
    }
    
    return number2;  
  }
  //method for boolean
 public static boolean userWins(int[] user, int[] winning){
   boolean win;
   if (user == winning){
     System.out.println("You win");
     win = true;
   }
   else {
     System.out.println("You lose");
     win = false;
   }
   return win;
 }

  public static void main(String[] args) {
    int[] winningNumb = numbersPicked();
    int[] userNumb = userPicked();
    System.out.print("Enter 5 numbers between 0 and 59: ");
    for (int j = 0; j < 5; j++) {
      System.out.print(userNumb[j]);
      if (j < 4) {
      System.out.print(", ");
      }
    }
    System.out.println();
    System.out.print("The winning numbers are: ");
    for (int i = 0; i < 5; i ++){
    System.out.print(winningNumb[i]);
      if (i < 4) {
    System.out.print(", ");
      }
    }
    System.out.println();
    System.out.println(userWins(userNumb, winningNumb));
  }
}
