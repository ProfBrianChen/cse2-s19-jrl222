//////////////////////
//// CSE2 HOMEWORK 5
//// Jared Lee
//// 3/5/2019
//// This program will ask 6 questions about a course you take and will make you put in inputs to answer them
import java.util.Scanner; //Need this to use Scanner method

public class Hw05 {
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    //intialize the integer i
    int i = 0;    
    System.out.println("What is your course number?");
    if(myScanner.hasNextInt()) {
      i = myScanner.nextInt();
    } else {
      //If the input is not an integer, the code will state that it is an incorrect input and ask to put in an integer.
      while(!myScanner.hasNextInt()) {
        System.out.println("This is the incorrect input, put in an integer.");
        myScanner.next();
      }
      i = myScanner.nextInt();
    }
    
    System.out.println(i);
    
    //intitialize the string dn
    System.out.println("What is the department name? ");
    String dn = myScanner.next();
    dn = dn + " " + myScanner.next();
    System.out.println(dn);
    
    //intialize the integer w
    int w = 0;
    System.out.println("How many times do you meet in a week? ");
    if(myScanner.hasNextInt()) {
      w = myScanner.nextInt();
    } else {
      while(!myScanner.hasNextInt()) {
        //If the input is not an integer, the code will state that it is an incorrect input and ask to put in an integer.
        System.out.println("This is the incorrect input, put in an integer.");
        myScanner.next();
      }
      w = myScanner.nextInt();
    }
    
    System.out.println(w);
    
    //initialize the string time
    String time;
    System.out.println("What time does the class start? ");
    if(myScanner.hasNext()) {
      time = myScanner.next();
    } else {
      while(!myScanner.hasNext()) {
        System.out.println("This is the incorrect input, put in a string.");
        myScanner.next();
      }
      time = myScanner.next();
    }
    
    System.out.println(time);
    
    //intialize the string name
    System.out.println("What is the instructor's name?" );
    String name = myScanner.next();
    name = name + " " + myScanner.next();
    System.out.println(name);
   
    
    //intialize the integer s
    int s = 0;
    System.out.println("What is the number of students in the class? ");
    if(myScanner.hasNextInt()) {
      s = myScanner.nextInt();
    } else {
      while(!myScanner.hasNextInt()) {
        //If the input is not an integer, the code will state that it is an incorrect input and ask to put in an integer.
        System.out.println("This is the wrong input, put in an integer.");
        myScanner.next();
      }
      s = myScanner.nextInt();
    }
    
    System.out.println(s);
    } //End of main method
  
} //End of class method
 
 
