HW5 Rubric

Total Score: 95

  75/75: Program compiles

  20/25: At least 5 of these are correct:
    course number: good
    department name: needs two inputs
    the number of times it meets in a week: good
    the time the class starts: could do a better way
    the instructor name: needs two inputs
    and the number of students: good

