///////////////////////////
/// CSE2 HW10
/// Jared Lee
/// 5/2/2019
/// This lab 
import java.util.Random;

public class RobotCity{
  
  public static int[][] buildCity(){
    Random random = new Random();
    int size = random.nextInt(6) + 10;
    int size2 = random.nextInt(6) + 10;
    int blocks = random.nextInt(900) + 100;
    int[][] cityArray = new int[size][size2];
    for (int i = 0; i < size; i++){
      for (int j = 0; j < size2; j++){
        cityArray[i][j] = random.nextInt(900) + 100; 
      }
    }
    
    return cityArray;
  }
  
  public static void display(int[][] cityArray){
    int[][] cityArray2 = new int[cityArray.length][];
    for (int i = 0; i < cityArray.length; i++){
      cityArray2[i] = cityArray[i];
      for (int j = 0; j < cityArray[i].length; j++){
        cityArray2[i][j] = cityArray[i][j];
      }
    }
    
    for (int m = 0; m < cityArray.length; m++){
      for (int n = 0; n < cityArray[m].length; n++){
        
        
        
        if (n == cityArray[m].length - 1){
          System.out.printf("  %3d      \n", cityArray2[m][n]);
        }
        else{
          System.out.printf("  %3d", cityArray2[m][n]);
        }
      }
    }
  }
         
         
  public static int[][] invade(int[][] cityArray, int k){
    Random random2 = new Random();
    int[][] cityArray3 = new int[cityArray.length][];
    for (int i = 0; i < cityArray.length; i++){
      cityArray3[i] = cityArray[i];
      for (int j = 0; j < cityArray[i].length; j++){
        cityArray3[i][j] = cityArray[i][j];
      }
    }
    
    for (int m = 0; m < k; m++){ 
        int size1 = random2.nextInt(cityArray.length -1);
        int size2 = random2.nextInt(cityArray[size1].length);
        cityArray3[size1][size2] = cityArray[size1][size2] * -1;
      }
    
    return cityArray3;
  }
         
  public static int[][] update(int[][] cityArray){
    int[][] cityArray4 = new int[cityArray.length][];
    for (int i = 0; i < cityArray.length; i++){
      cityArray4[i] = cityArray[i];
      for (int j = 0; j < cityArray[i].length; j++){
        cityArray4[i][j] = cityArray[i][j];
      }
    }
    
    
    for (int i = 0; i < cityArray4.length; i++){
      for (int j = cityArray4[i].length -1; j > -1; j--){
        
    if((cityArray4[i][j] < 0) && ((j != cityArray4[i].length - 2))){
          cityArray4[i][j] = cityArray4[i][j] * -1;
          cityArray4[i][j+1] = cityArray4[i][j+1] * -1;
        }
    if((cityArray4[i][j] < 0) && (j == cityArray4[i].length -2) ){
          cityArray4[i][j] = cityArray4[i][j] * -1;
        }
      }
    }
    return cityArray4;
  }
  
  public static void main(String[] args){
    Random random3 = new Random();
    int r = random3.nextInt(10) + 1;
    int[][] j = buildCity();
    display(j);
    int[][] k = invade(j, r);
    System.out.println();
    display(k);
    System.out.println();
    System.out.println("update:");
    System.out.println();
   
    int[][] m = update(k);
    display(m);
    System.out.println();
    
    int[][] n = update(m);
    display(n);
    System.out.println();
    
    int[][] o = update(n);
    display(o);
    System.out.println();     

    int[][] p = update(o);
    display(p);
    System.out.println();
    
    int[][] q = update(p);
    display(q);
    System.out.println();
  }
}
