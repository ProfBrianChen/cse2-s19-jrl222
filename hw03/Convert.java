///////////////////////
//// CSE 02 HW 03
//// Jared Lee
//// 2/12/2019
//// This program will convert meters into inches
import java.util.Scanner;

public class Convert {
  // main method required for every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print( "Enter the distance in meters: ");
    //input the distance in meters
    double meters = myScanner.nextDouble();
    // 1 meter = 39.37 inches
    double inches = (meters * 39.37007888) *10000; 
    //multipy by 10000, convett inches into a integer variable, and divide by 10000.0 
    // to get exactly four decimal places.
    inches = (int) inches;
    inches = inches / 10000.0;
    System.out.println( meters + " meters is " + inches + " inches.");
    
    
  }//end of main method
  
  
}//end of class