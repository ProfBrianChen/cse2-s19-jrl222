/////////////////////////////
//// CSE 002 HW03 PART II
//// Jared Lee
//// 2/12/2019
//// This program will calculate the volume of the box using
//// inputted width, length, and height of the box
import java.util.Scanner;

public class BoxVolume {
  // main method required for every java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print( "The width side of the box is: ");
    //input the width
    int width = myScanner.nextInt();
    System.out.print( "The length of the box is: ");
    //input the length
    int length = myScanner.nextInt();
    System.out.print( "The height of the box is: ");
    //input the height
    int height = myScanner.nextInt();
    //make volume an integer variable
    int volume;
    volume = height * length * width;
    System.out.println( "The volume inside the box is: " + volume );
  }// end of main method
}//end of class
