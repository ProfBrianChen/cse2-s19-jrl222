/////////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{ 

  public static void main(String args[]){
    //In order to make my code look exactly like the hw, I added extra spaces
    System.out.println("  -----------"); 
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    /*In order to print without illegal escape characters,
      I put an extra backslash, which are special characters,
      after each backslash I put in */
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--R--L--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    
  }

}