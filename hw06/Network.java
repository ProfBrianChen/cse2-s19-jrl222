///////////////////////////
//// CSE2 HW06
//// Jared Lee
//// 3/19/2019
//// This program will create a network of squares depending on the input  
// Create a scanner method so we can put inputs in
import java.util.Scanner;

public class Network {
  public static void main(String[] args) {
    Scanner scan = new Scanner( System.in );
    
    //Print a line asking for a height of the code
    System.out.println("Input your desired height: ");
      int height = 0;
    while (height == 0) {
       if (scan.hasNextInt()) {
         height = scan.nextInt();
         //Make sure height input is not a negative
         if (height < 0) {
         System.out.println("Error: please type in an integer. ");
         height = 0;
       }  
     }
     //Make sure height input is only an integer
     else {
       System.out.println("Please reenter an integer.");
       String junk = scan.next();
      }
    }
    //Print a line asking for a width of the code 
    System.out.println("Input your desired width: ");
      int width = 0;
    while (width == 0) {
       if (scan.hasNextInt()) {
         width = scan.nextInt();
         //Make sure the width input is not negative
         if (width < 0) {
         System.out.println("Error: please type in an integer. ");
         width = 0;
       }  
     }
     //Make sure the input for width is actually an integer
     else {
       System.out.println("Please reenter an integer.");
       String junk = scan.next();
      }
    }
    //Print a line asking for size of the square for the code
    System.out.println("Input square size: ");
      int square = 0;
    while (square == 0) {
       if (scan.hasNextInt()) {
         square = scan.nextInt();
         //Make sure the input of the square size is not negative
         if (square < 0) {
         System.out.println("Error: please type in an integer. ");
         square = 0;
       }  
     }
      //Make sure the square size input is an integer and nothing else
     else { 
       System.out.println("Please reenter an integer.");
       String junk = scan.next();
      }
    }
    // Print a line asking for edge length 
    System.out.println("Input edge length: ");
      int edge = 0;
    while (edge == 0) {
       if (scan.hasNextInt()) {
         edge = scan.nextInt();
         //Make sure the input for edge length is not negative
         if (edge < 0) {
         System.out.println("Error: please type in an integer. ");
         edge = 0;
       }  
     }
     //Make sure the input is integer and nothing else
     else {
       System.out.println("Please reenter an integer.");
       String junk = scan.next();
      }
    }
    //initialize i and j. i is connected to column and j is connected to row.
    int i = 0;
    int j = 0;
    //Create while loop for when the number of rows is less than the height input 
    while(j<height) { 
      while(j%(square+edge)<square) {
        i=0;
        //i = 0 makes sure that the code can reset and make another square from scratch
        while(i<=width) {
          //When the first row and the last row are equal to j, # is printed
          //Otherwise a | is printed 
          if(j%(square+edge)==0||j%(square+edge)==square-1) {
            System.out.print("#");
          } else {
            System.out.print("|");
          }
          i++;
          //Whenever incrementing i, make sure i is not larger than the width input 
          if(i>=width) {
            break;
          }
          //This while statement will print the horizontal dashes on the top and bottom of the square
          while(i%(square+edge)<square-1) {
            if(j%(square+edge)==0||j%(square+edge)==square-1) {
              System.out.print("-");
            } else {
              System.out.print(" ");
            }
            i++;
            //Whenever incrementing i, make sure i is not larger than the width input 
            if(i>=width) {
              break;
            }
          }
          //Print whether row should first print # or |
          if(j%(square+edge)==0||j%(square+edge)==square-1) {
            System.out.print("#");
          } else {
            System.out.print("|");
          }
          i++;
          //Whenever incrementing i, make sure i is not larger than the width input 
          if(i>=width) {
            break;
          }
          //While loop for edge length
          while(i%(square+edge)>=square) {
            //Use if statement to check if square odd
            if(square%2==1) {
              if(j%(square+edge)==square/2) {
                System.out.print("-");
              } else {
                System.out.print(" ");
              }
              //If square is even 
            } else {
              if(j%(square+edge)==square/2||j%(square+edge)==square/2-1) {
                System.out.print("-");
              } else {
                System.out.print(" ");
              }
            }
            i++;
            //Whenever incrementing i, make sure i is not larger than the width input 
            if(i>=width) {
              break;
            }
         }
        }
        System.out.println();
        j++; //increment j
      }
      while(j%(square+edge)>=square) {
        //Make sure the code breaks if j is equal or greater than j
        if(j>=height) {
          break;
        }
        i=0;
        while(i<width) {
          //If statement makes sure that the square is even or odd
          if(square%2==1) {
            if(i%(square+edge)==square/2) {
              //This if statement prints the vertical edge length of the odd box
              System.out.print("|");
            } else {
              //If the column is not in the middle of square 
              System.out.print(" ");
            }
          } else {
            //This else statement represents the code where the square size is even
            if((i%(square+edge)==square/2)||(i%(square+edge)==square/2-1)) {
              System.out.print("|");
            } else {
              System.out.print(" ");
            }
          }
          i++;
          //Whenever incrementing i, make sure i is not larger than the width input 
          if(i>=width) {
            break;
          }
        }
        System.out.println();
        j++;
      }
    }
  } // end of main method
} // end of class method