/////////////////////////////////////////
//// CSE2 LAB09
//// Jared Lee
//// 4/12/2019
//// This lab will 
import java.util.Scanner;
import java.util.Random;

public class lab09{
  
  public static void methodA(int size, int searchInt){
    Random random = new Random();
    int[] array1 = new int[size];
    for (int i = 0; i < size; i++){
      int integer1 = random.nextInt(size);
      array1[i] = integer1;
      System.out.println("Array [" + i + "]: " + array1[i]);
    }
    System.out.println();
    int searchInt2 = searchInt;
    methodC(array1, searchInt2);
    
  }
    
  public static void methodB(){
    
  }
  
  public static void methodC(int array1[], int searchInt){
    int size = array1.length;
    int[] array2 = new int[size];
    for (int i = 0; i < size; i++){
      array2[i] = array1[i];
    }
    int searchInt2 = searchInt;
    if (size <= searchInt2) {
      System.out.println("Integer not found in index:" + -1);
    }
    else {
    System.out.print("Array member [" + searchInt2 + "] is " + array2[searchInt2]);
    }
  }
  
  public static void methodD(){
    
  }
  
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Would you like to perform a linear search or a binary search?"); 
    System.out.print("(Enter either 'linear' or 'binary'): ");
    String search = scan.next();
    if (search.equals("linear")){
      System.out.print("Enter a size for the array: ");
      int size1 = scan.nextInt();
      System.out.print("Enter an integer search term: ");
      int searchInt1 = scan.nextInt();
      methodA(size1, searchInt1);
    }
    else if (search.equals("binary")) {
      System.out.print("Hello");
    }
    else {
      System.out.println("Please reenter type of search: ");
      search = scan.next();
      return;
    }
  }
}