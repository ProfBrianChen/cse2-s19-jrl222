/////////////////////////////////////////
//// CSE2 LAB09
//// Jared Lee
//// 4/12/2019
//// This lab will  ask the user for a type of search. Once the user enters either 'linear' or 'binary', there will be a random array generated (ascending integers for binary)
/// Then the array will printed as well as the array member that contains the same integer as the input
import java.util.Scanner;
import java.util.Random;

public class Searching{
  //methodA is a method that will create an array for linear search. The integers are random for each member
  public static void methodA(int size, int searchInt){
    Random random = new Random();//random generator
    int[] array1 = new int[size];//declare and create an array
    for (int i = 0; i < size; i++){
      int integer1 = random.nextInt(size);
      array1[i] = integer1;
      System.out.println("Array [" + i + "]: " + array1[i]);
    }
    System.out.println();
    int searchInt2 = searchInt;
    methodC(array1, searchInt2);
    
  }
  //methodB creates an array for the binary search. The integers are ascending as array members are beinf filled  
  public static void methodB(int size, int searchInt){
    Random random = new Random();
    int[] array1 = new int[size];
    int min = array1[0];
    for (int i = 0; i < size; i++){
      int integer1 = random.nextInt(size);
      array1[i] = integer1;
      //use for statement to see if the current member has an integer that is greater than the integers in the previous members
      for (int j = 0; j < i; j++){
        if (array1[j] > array1[i]){
          int small = array1[i];
          int big = array1[j];
          array1[i] = big;
          array1[j] = small;
        }
      }
    }
    for (int i = 0; i < size; i++){
      System.out.println("Array [" + i + "]: " + array1[i]);
    }
    System.out.println();
    int searchInt2 = searchInt;
    methodD(array1, searchInt2);
    
  }
  //MethodC will actually do a linear search in the array and see if any of the array members contain an integer equal to the integer input
  public static void methodC(int array1[], int searchInt){
    int size = array1.length;
    int[] array2 = new int[size];
    int searchInt2 = searchInt; 
    int count = 0;//use count to see if integer is in array index or not
    for (int i = 0; i < size; i++){
      array2[i] = array1[i];
      if (array2[i] == searchInt2){
        System.out.print("Array member [" + i + "] is " + array2[i]);
        count = count + 1;
      }
    }
    if (count == 0){
      System.out.println("Integer not found in index:" + -1);
    }
  
  }
  //MethodD will actually do a binary search in the array and see if any of the array members contain an integer equal to the integer input
  public static void methodD(int array1[], int searchInt){
    int size = array1.length;
    int[] array2 = new int[size];
    int searchInt2 = searchInt; 
    int count = 0;//use count to tell if array contains integer input
    for (int i = 0; i < size; i++){
      array2[i] = array1[i];
      if (array2[i] == searchInt2){
        System.out.print("Array member [" + i + "] is " + array2[i]);
        count = count + 1;
      }
    }
    if (count == 0){//if count never changes, then that means integer was never in index
      System.out.println("Integer not found in index:" + -1);
  }
  }
  //main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Would you like to perform a linear search or a binary search?"); 
    System.out.print("(Enter either 'linear' or 'binary'): ");
    String search = scan.next();
    while(true){
      //if linear is entered
      if (search.equals("linear")){
        System.out.print("Enter a size for the array: ");
        int size1 = scan.nextInt();
        System.out.print("Enter an integer search term: ");
        int searchInt1 = scan.nextInt();
        
        methodA(size1, searchInt1);
        System.out.println();
        return;
      }
      //if user enters binary
      else if (search.equals("binary")) {
        System.out.print("Enter a size for the array: ");
        int size1 = scan.nextInt();
        System.out.print("Enter an integer search term: ");
        int searchInt1 = scan.nextInt();
        methodB(size1, searchInt1);
        System.out.println();
        return;
      }
      //if neither binary or linear are entered
      else {
        System.out.println("Please reenter type of search: ");
        search = scan.next();
      }
  }
  }//end of main method
}//end of class method