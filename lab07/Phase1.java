/////////////////////////////////
///// CSE2 LAB07
///// Jared Lee
///// 3/22/2019
///// I don't know what to expect from this lab yet

import java.util.Random;
public class Phase1 {
  public static String myAdjective() {
    String adjective = "";
    Random randomGenerator = new Random();
    int randomInt1 = randomGenerator.nextInt(10);
    switch ( randomInt1 ) {
      case 0:
        adjective = "small";
        break;  
      case 1:
        adjective = "little";
        break;
      case 2:
        adjective = "tiny";
        break;
      case 3:
        adjective = "yellow";
        break;
      case 4:
        adjective = "big";
        break;
      case 5:
        adjective = "large";
        break;
      case 6:
        adjective = "huge";
        break;
      case 7:
        adjective = "giant";
        break;
      case 8:
        adjective = "dirty";
        break;
      case 9:
        adjective = "clean";
        break;
    }
    return adjective; 
  }
  
  public static String myVerb() {
    String verb = "";
    Random randomGenerator = new Random();
    int randomInt2 = randomGenerator.nextInt(10);
    switch ( randomInt2 ) {
      case 0:
        verb = "smelled";
        break;
      case 1:
        verb = "touched";
        break;
      case 2:
        verb = "felt";
        break;
      case 3:
        verb = "saw";
        break;
      case 4:
        verb = "sat";
        break;
      case 5:
        verb = "marked";
        break;
      case 6:
        verb = "stuck";
        break;
      case 7:
        verb = "pushed";
        break;
      case 8:
        verb = "kicked";
        break;
      case 9:
        verb = "punched";
        break;
    }
    return verb; 
  }
  
  public static String myNoun1() {
    String noun = "";
    Random randomGenerator = new Random();
    int randomInt3 = randomGenerator.nextInt(10);
    switch ( randomInt3 ) {
      case 0:
        noun = "apple";
        break;
      case 1:
        noun = "banana";
        break;
      case 2:
        noun = "cherry";
        break;
      case 3:
        noun = "pear";
        break;
      case 4:
        noun = "blueberry";
        break;
      case 5:
        noun = "lime";
        break;
      case 6:
        noun = "lemon";
        break;
      case 7:
        noun = "peach";
        break;
      case 8:
        noun = "kiwi";
        break;
      case 9:
        noun = "grape";
        break;
    }
    return noun ; 
  }
  
  public static String myNoun2() {
    String object = "";
    Random randomGenerator = new Random();
    int randomInt4 = randomGenerator.nextInt(10);
    switch ( randomInt4 ) {
      case 0:
        object = "carrot";
        break;
      case 1:
        object = "cucumber";
        break;
      case 2:
        object = "tomato";
        break;
      case 3:
        object = "leek";
        break;
      case 4:
        object = "potato";
        break;
      case 5:
        object = "pepper";
        break;
      case 6:
        object = "radish";
        break;
      case 7:
        object = "onion";
        break;
      case 8:
        object = "mushroom";
        break;
      case 9:
        object = "rudabega";
        break;
    }
    return object; 
  }  
  
  public static void main(String[] args) {
    String b = myVerb();
    String c = myNoun1();
    String d = myNoun2();
    System.out.println("The " + myAdjective() + " " + myAdjective() + " " + c + " " + b + " the " + myAdjective() + " " + d + ".");
    
  }
}